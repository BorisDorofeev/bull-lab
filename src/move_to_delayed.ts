import { Queue, Worker, QueueScheduler } from 'bullmq';

export const queueSettings = {
  name: 'test_queue_lab',
};

const scheduler = new QueueScheduler(queueSettings.name);

export const queue = new Queue(queueSettings.name);

queue.add('job_run_onсe', { payload: 'some data' });

const worker = new Worker(queueSettings.name, async (job) => {
  if (job.name === 'job_run_onсe') {
    return new Promise((resolve) => {
      setTimeout(async () => {
        if (!job.returnvalue) {
          console.log('job complete, delay first time however...');
          await job.moveToDelayed(Date.now() + 3000);
          return resolve({ result: 'almost...', calls: 1 });
        } else {
          console.log('job complete!');
          return resolve({ result: 'ok..!', calls: 2 });
        }
      }, 1000);
    });
  }
});

// metricsWorker.on('completed', async (job) => {
//   if (job.returnvalue.calls === 1) {
//     await job.moveToDelayed(Date.now() + 3000);
//   }
// });

setTimeout(async () => {
  await worker.close();
  await scheduler.close();
  await queue.close();
  process.exit();
}, 8000);
