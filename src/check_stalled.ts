import { Queue, Worker, QueueScheduler, QueueEvents } from 'bullmq';
import { after } from 'lodash';
import { delay, removeAllQueueData } from './utils';
import IORedis = require('ioredis');
//import sbox from './sanboxProcessor';

const concurrency = 4;
const name = 'test_queue_lab';

(async () => {
  await removeAllQueueData(new IORedis(), name);

  const queue = new Queue(name);
  await queue.waitUntilReady();

  const queueEvents = new QueueEvents(name);
  await queueEvents.waitUntilReady();

  const worker = new Worker(
    name,
    async (job) => {
      // if (job.data.lockJob) {
      //   const end = Date.now() + 1000;
      //   while (Date.now() < end) {
      //     const heavy = 2 * 2;
      //   }
      // }

      return delay(3000);
    },
    {
      lockDuration: 1000000, //умолчание - половина жизни лока, можно просто увеличить больше предполагаемой задержки
      //lockRenewTime: 2000, //если сделать обновление лока сильно НЕзаранее, то QueueScheduler заберет в stalled
      concurrency,
    }
  );

  //const processFile = __dirname + '/sanboxProcessor.ts';
  // const sbWorker = new Worker(name, sbox, {
  //   settings: {
  //     //guardInterval: 300000,
  //     stalledInterval: 300000,
  //   },
  // });

  const allActive = new Promise((resolve) => {
    worker.on('active', after(concurrency, resolve));
  });

  await worker.waitUntilReady();

  const jobs = await Promise.all([
    queue.add('test-1', {}),
    queue.add('test-2', {}),
    queue.add('test-3', {}),
    queue.add('test-4', {}),
  ]);

  await allActive;
  console.log('all active!');

  //process.exit();

  queue.add('test-busy', { lockJob: true });

  const queueScheduler = new QueueScheduler(name, {
    stalledInterval: 100,
    maxStalledCount: 0,
  });
  await queueScheduler.waitUntilReady();

  queueScheduler.on('failed', (job, err) => {
    console.log('failed! - ' + job + ': ' + err.message);
  });

  queueScheduler.on('stalled', async (job) => {
    console.log('stalled! - ' + job);
    // const joB = await queue.getJob(job);
    // await joB.moveToDelayed(Date.now() + 1000);
  });

  worker.on('completed', (job) => {
    console.log('completed! - ' + job.id);
  });

  queueEvents.on('delayed', (job) => {
    console.log('delayed! - ' + job.jobId);
  });

  //await worker.close(true); //если закрыть здесь - воркер потеряет лок и всех соберет QueueScheduler как заглохших

  const allFailed = new Promise((resolve) => {
    queueScheduler.on('failed', after(concurrency, resolve));
  });

  const allCompleted = new Promise((resolve) => {
    worker.on('completed', after(concurrency, resolve));
  });

  // const processFile = __dirname + '/sanboxProcessor.ts';
  // const sbWorker = new Worker(name, processFile, {
  //   settings: {
  //     //guardInterval: 300000,
  //     stalledInterval: 300000,
  //   },
  // });
  // await sbWorker.waitUntilReady();

  await Promise.race([allFailed, allCompleted]);

  // await new Promise((resolve, reject) => {
  //   sbWorker.on('completed', async (job, value) => {
  //     resolve();
  //   });
  //   sbWorker.on('failed', async (job, value) => {
  //     resolve();
  //   });
  // });

  console.log('The end.');

  await worker.close();
  await queueScheduler.close();
  await queue.close();
})();
